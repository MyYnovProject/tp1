# Prérequis
	
*Partitionnement*

```
pvcreate /dev/sdb
vgcreate data /dev/sdb
lvcreate -L 2G data -n my_data
lvcreate -L 2.5G data -n your_data
mkfs -t ext4 /dev/data/my_data
mkfs -t ext4 /dev/data/your_data
mkdir /srv/data1
mkdir /srv/data2
mount /dev/data/my_data /srv/site1
mount /dev/data/ypour_data /srv/site2
```

```
[master@localhost ~]# lvs
                                            
  HOME data   -wi-ao----  2.50g                                                    
  SRV  data   -wi-ao----  2.00g

[master@localhost ~]# cat /etc/fstab 

/dev/data/my_data /srv/data1 ext4 defaults 0 0
/dev/data/your_data /srv/data2 ext4 defaults 0 0

[master@node1 ~]# mount -av

/srv/data1               : already mounted
/srv/data2               : already mounted
```

										   

 *Connexion*


     [master@node1 ~]$ ip a
        
     2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
            link/ether 08:00:27:24:8f:35 brd ff:ff:ff:ff:ff:ff
     3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
            link/ether 08:00:27:21:00:6e brd ff:ff:ff:ff:ff:ff
            inet 192.168.1.11/24 brd 192.168.1.255 scope global noprefixroute enp0s8
               valid_lft forever preferred_lft forever
            inet6 fe80::a00:27ff:fe21:6e/64 scope link 
               valid_lft forever preferred_lft forever

*SSH*

    [admin@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
    NAME=enp0s8
    DEVICE=enp0s8
    
    BOOTPROTO=static
    ONBOOT=yes
    
    IPADDR=192.168.1.11
    NETMASK=255.255.255.0

```


      [master@node1 ~]$ systemctl status sshd
        ● sshd.service - OpenSSH server daemon
           Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
           Active: active (running) since Mon 2020-09-28 20:44:12 CEST; 3h 12min ago
             Docs: man:sshd(8)
                   man:sshd_config(5)
         Main PID: 1637 (sshd)
           CGroup: /system.slice/sshd.service
                   └─1637 /usr/sbin/sshd -D
        
        Sep 28 20:44:12 node1.tp1.b2 systemd[1]: Stopped OpenSSH server daemon.
        Sep 28 20:44:12 node1.tp1.b2 systemd[1]: Starting OpenSSH server daemon...
        Sep 28 20:44:12 node1.tp1.b2 sshd[1637]: Server listening on 0.0.0.0 port 22.
        Sep 28 20:44:12 node1.tp1.b2 systemd[1]: Started OpenSSH server daemon.
        Sep 28 20:47:26 node1.tp1.b2 sshd[1641]: Connection closed by 192.168.1.1 port 50620 [preauth]
        Sep 28 20:47:59 node1.tp1.b2 sshd[1644]: Accepted password for master from 192.168.1.1 port 50622 ssh2


Modification du fichier  `/etc/ssh/sshd_config`
    
    PubkeyAuthentication yes

```


 *Local*
										 
    [master@node1 ~]$ ip r s
    192.168.1.0/24 dev enp0s8 proto kernel scope link src 192.168.1.11 metric 100 

*Hostname*

    [master@node1 ~]$ hostname
    node1.tp1.b2
	

    [master@node1 ~]$ ping -c 2 node1.tp1.b2
    PING node1.tp1.b2 (192.168.1.11) 56(84) bytes of data.
    64 bytes from node1.tp1.b2 (192.168.1.11): icmp_seq=1 ttl=64 time=0.098 ms
    64 bytes from node1.tp1.b2 (192.168.1.11): icmp_seq=2 ttl=64 time=0.127 ms
    
    --- node1.tp1.b2 ping statistics ---
    2 packets transmitted, 2 received, 0% packet loss, time 999ms
    rtt min/avg/max/mdev = 0.098/0.112/0.127/0.017 ms

*User*

 

    ```
    [master@node1 ~]# useradd master
    
    ```
    
    (master ALL=(ALL) ALL").

*Firewall*

    [master@node1 ~]# firewall-cmd --add-port=22/tcp --permanent
    [master@node1 ~]# firewall-cmd --reload
    [master@node1 ~]# systemctl start firewalld


# Serveur Web

*Install*

    [master@node1 ~]# yum install epel-release
    [master@node1 ~]# yum install nginx
    

 *Configurations nginx *

        user nginx;
        worker_processes auto;
        error_log /var/log/nginx/error.log;
        pid /run/nginx.pid;                                                  
        include /usr/share/nginx/modules/*.conf;
        
        events {
            worker_connections 1024;
        }
        
        http {
            log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                              '$status $body_bytes_sent "$http_referer" '
                              '"$http_user_agent" "$http_x_forwarded_for"';
        
            access_log  /var/log/nginx/access.log  main;
        
            sendfile            on;
            tcp_nopush          on;
            tcp_nodelay         on;
            keepalive_timeout   65;
            types_hash_max_size 2048;
        
            include             /etc/nginx/mime.types;
            default_type        application/octet-stream;
        
            # Load modular configuration files from the /etc/nginx/conf.d directory.                                         
            # See http://nginx.org/en/docs/ngx_core_module.html#include                                                      
            # for more information.                                                                                          
            include /etc/nginx/conf.d/*.conf;
            
            server {
                listen       80 default_server;
                listen       [::]:80 default_server;
                server_name  node1.tp1.site1;
        
                # Load configuration files for the default server block.                                                     
                include /etc/nginx/default.d/*.conf;
        
                location / {
                         root /srv/site1;
                         index index.html index.htm index.nginx-debian.html;
                }
        
                error_page 404 /404.html;
                    location = /40x.html {
        	}
        
                error_page 500 502 503 504 /50x.html;
                    location = /50x.html {
                }
            }
            server {
                listen       80;
                listen       [::]:80;
                server_name  node1.tp1.site2;
        
                # Load configuration files for the default server block.                                                    \
                                                                                                                             
                include /etc/nginx/default.d/*.conf;
        
                location / {
                         root /srv/site2;
                         index index.html index.htm index.nginx-debian.html;
                         }
        
                error_page 404 /404.html;
                    location = /40x.html {
                }
        
                error_page 500 502 503 504 /50x.html;
                    location = /50x.html {
                }
            }
    
    server {
           listen       443 ssl http2 default_server;
           listen       [::]:443 ssl http2 default_server;
           server_name  node1.tp1.site1;
           root         /usr/share/nginx/html;
    
           ssl_certificate node1.tp1.site1.crt;
           ssl_certificate_key node1.tp1.site1.key;
           ssl_session_cache shared:SSL:1m;
           ssl_session_timeout  10m;
           ssl_ciphers HIGH:!aNULL:!MD5;
           ssl_prefer_server_ciphers on;
    
           # Load configuration files for the default server block.                                                                                             
           include /etc/nginx/default.d/*.conf;
    
           location / {
                    root /srv/site1;
                    index index.html index.htm index.nginx-debian.html;
                    }
    
           error_page 404 /404.html;
               location = /40x.html {
           }
    
           error_page 500 502 503 504 /50x.html;
               location = /50x.html {
           }
        }
    
        server {
           listen       443 ssl http2;
           listen       [::]:443 ssl http2;
           server_name  node1.tp1.site2;
           root         /usr/share/nginx/html;
    
           ssl_certificate node1.tp1.site2.crt;
           ssl_certificate_key node1.tp1.site2.key;
           ssl_session_cache shared:SSL:1m;
           ssl_session_timeout  10m;
           ssl_ciphers HIGH:!aNULL:!MD5;
           ssl_prefer_server_ciphers on;
    
           # Load configuration files for the default server block.                                                                                             
           include /etc/nginx/default.d/*.conf;
    
           location / {
                    root /srv/site2;
                    index index.html index.htm index.nginx-debian.html;
                    }
    
           error_page 404 /404.html;
               location = /40x.html {
           }
    
           error_page 500 502 503 504 /50x.html;
               location = /50x.html {
           }
        }
    }

*Ouverture de port 80 / 443 https et reload*

    [master@node1 srv]# firewall-cmd --add-port=80/tcp --permanent
    [master@node1 srv]# firewall-cmd --add-port=443/tcp --permanent
    [master@node1 srv]# firewall-cmd --reload
    [master@node1 srv]# systemctl restart firewalldl

*Test HTTPS*

    [master@node2 ~]$ curl -kL https://node1.tp1.site1/
    <!DOCTYPE html>
    <html>
    <body>
    
    <h1>Fisrt Site WebPage</h1>
    <p>the choosen one.</p>
    
    </body>
    </html>
    
    [master@node2 ~]$ curl -kL https://node1.tp1.site2/
    <!DOCTYPE html>
    <html>
    <body>
    
    <h1>Second Site WebPage</h1>
    <p>the sencond one.</p>
    
    </body>
    </html>
